class HomesController < ApplicationController
	
	before_filter :check_logged_in_user, :only => [:show]

	def show
	end

	def about
	end

	def user
		redirect_to current_user
	end

	private

	def check_logged_in_user
		if signed_in?
			redirect_to dashboard_path
		end
	end
end


