class FeedsController < ApplicationController
    before_action :find_feed, :only =>[:destroy,:destroy_all_items,:update]
	def destroy
		if @feed.user == current_user
			@feed.destroy
			redirect_to news_items_path

		else
			flash.alert = 'Could not delete feed.'
			redirect_to news_items_path
		end
	end	

	def create
  		feed = current_user.feeds.build feed_parameters
  		if feed.save
			redirect_to news_items_path
		else
			flash.alert = 'Could not add feed.'
			redirect_to news_items_path
		end
	end

	def destroy_all_items
		@feed.news_items.delete_all
		redirect_to news_items_path		
	end

	def update
		if @feed.user == current_user
			@feed.update
			redirect_to news_items_path

		else
			flash.alert = 'Could not update feed.'
			redirect_to news_items_path
		end
		
	end


	private


	def feed_parameters
		params.require(:feed).permit(:url, :name)
	end

	def find_feed
		@feed = Feed.find(params[:id])
	end


end
