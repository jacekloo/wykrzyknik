class NewsItemsController < ApplicationController
  def index
  	feeds = Feed.where(user_id: current_user).pluck(:id)
  	@news_items = NewsItem.where(feed_id: feeds)
  	@has_feeds = current_user.feeds.present? ? true : false
  end

  def show
  end

  def destroy
  	@news_item = NewsItem.find(params[:id])
  	@news_item.destroy
	redirect_to news_items_path
 	
  end
end
