require 'digest/md5'

module YellsHelper
	def gravatar user, size = 12
		digest = Digest::MD5.hexdigest(user.email)
		image_tag('http://gravatar.com/avatar/#{digest}?s=#{size}', class: 'gravatar')
	end
end