class Feed < ActiveRecord::Base
  belongs_to :user
  has_many :news_items
  default_scope { order('created_at DESC')}
  validates_presence_of :name
  validates_presence_of :url




  def self.update_feeds
  	feeds = Feed.all
    	feeds.each do |feed|
    	feed.update
    	end
  end

  def update
	feed = Feedzirra::Feed.fetch_and_parse(url)
  	feed.entries.each do |entry|
  		news_item = news_items.build
  		news_item.title = entry.title
  		news_item.content = entry.summary.sanitize
  		news_item.item_url = entry.url
  		news_item.feed = self

      news_item.save
  	end
  end
end
