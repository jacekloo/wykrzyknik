class PhotoYell < ActiveRecord::Base
	has_attached_file :image, styles: {
		yell: "200x200>"
	}
#	validates_attachment_presence :image
#   validates_with AttachmentPresenceValidator
	validates_attachment :image, :presence => true
end
