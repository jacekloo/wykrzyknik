class Timeline
	extend ActiveModel::Naming # nie działa a miało zastąpić metode to_partial_path

	def initialize user
		@user = user
	end

	def yells
		Yell.where(user_id: yell_user_ids)
	end

	def to_partial_path
		'timelines/timeline'
	end

	private

	def yell_user_ids
		[@user.id] + @user.followed_users.collect{|u| u.id}
	end
end