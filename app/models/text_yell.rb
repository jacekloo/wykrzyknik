class TextYell < ActiveRecord::Base
  validates :body, length: { minimum: 2 }
  end
