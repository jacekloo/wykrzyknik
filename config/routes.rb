Wykrzyknik::Application.routes.draw do

  
  root to: 'homes#show', via: :get
  get 'about' => 'homes#about'

  resource :session, only: [:new, :create, :destroy]
  resource :dashboard, only: [:show]
  resources :users, only: [:new, :create, :show, :index] do
  	post 'follow' => 'following_relationships#create'
  	delete 'follow' => 'following_relationships#destroy'
  end

  get "/user/"=> "homes#user", as: :current_user

  resources :yells, only: [:show, :destroy]
  resources :text_yells, only: [:create]
  resources :photo_yells, only: [:create]

  resources :feeds, only: [:create, :destroy, :update] do
     member do
      get "destroy_all_items"
     end
  end
  resources :news_items, only: [:index, :destroy]


end
