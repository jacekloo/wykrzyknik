class AddFeedToNewsItem < ActiveRecord::Migration
  def change
    add_reference :news_items, :feed, index: true
  end
end
